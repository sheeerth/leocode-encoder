## How to run

### dev mode

```bash
npm install
npm run start
```

Server run in localhost:3000

### prod mode

```bash
npm install
npm run build
node dist/main.js
```

Author - Bartosz Gawroński
