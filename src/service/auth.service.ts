import { Injectable } from '@nestjs/common';
import * as md5 from 'md5';
import * as jwtNode from 'jsonwebtoken';
import { Request } from 'express';
import { env } from '../env';
import { UserMemoryRepository } from './user-memory-repository.service';

@Injectable()
export class AuthService {
  constructor(private userService: UserMemoryRepository) { }

  async signIn(email: string, password: string): Promise<{authToken: string} | null> {
    const user = await this.userService.findUser(email);

    if (user && user.password === md5(password)) {
      const jwt = jwtNode.sign({email}, env.SECRET_KEY, {expiresIn: 300});
      return {authToken: jwt};
    }

    return null;
  }

  async verificationAuth(request: Request): Promise<string | object> {
    return jwtNode.verify(request.headers.authorization, env.SECRET_KEY);
  }
}
