import { UserMemoryRepository } from './user-memory-repository.service';
import { UserRepository } from '../interface/UserRepository';
import * as jwtNode from 'jsonwebtoken';
import { env } from '../env';

describe('[USER MEMORY REPOSITORY]', () => {
  let userMemoryRepository: UserRepository;

  beforeEach(() => {
    userMemoryRepository = new UserMemoryRepository();
  });

  test('[USER MEMORY REPOSITORY] find user', async () => {
    expect(await userMemoryRepository.findUser('test@test.pl')).toStrictEqual({
      email: 'test@test.pl',
      password: '662af1cd1976f09a9f8cecc868ccc0a2',
    });
  });

  test('[USER MEMORY REPOSITORY]', async () => {
    const jwtMockUp = jwtNode.sign({email: 'test@test.pl'}, env.SECRET_KEY, {expiresIn: 300});

    expect(await userMemoryRepository.findUserByJwtToken(jwtMockUp)).toStrictEqual({
      email: 'test@test.pl',
      password: '662af1cd1976f09a9f8cecc868ccc0a2',
    });
  });
});
