import { Injectable } from '@nestjs/common';
import { User } from '../interface/user';
import * as jwtNode from 'jsonwebtoken';
import { UserRepository } from '../interface/UserRepository';

@Injectable()
export class UserMemoryRepository implements UserRepository {
  private readonly users: User[];

  constructor() {
    this.users = [];
    this.users.push(
      {
        email: 'test@test.pl',
        password: '662af1cd1976f09a9f8cecc868ccc0a2',
      },
      {
        email: 'leocode@test.pl',
        password: '662af1cd1976f09a9f8cecc868ccc0a2',
      },
    );
  }

  async findUser(email: string): Promise<User> {
    const userIndex = this.users.findIndex((user: User) => user.email === email);

    return this.users[userIndex];
  }

  async findUserByJwtToken(jwtToken: string): Promise<User> {
    // @ts-ignore
    return await this.findUser(jwtNode.decode(jwtToken).email);
  }
}
