import { EncryptService } from './encrypt.service';
import NodeRSA = require('node-rsa');
import { Keys } from '../interface/keys';

describe('[ENCRYPT SERVICE]', () => {
  let encryptService: EncryptService;

  beforeEach(() => {
    encryptService = new EncryptService();
  });

  test('[ENCRYPT SERVICE]  generate keys', () => {
    // TODO
    const rsaKeys = new NodeRSA();
    rsaKeys.generateKeyPair();

    const keys: Keys = {
      privKey: rsaKeys.exportKey('pkcs8-private-pem').replace(/\n/g, ''),
      pubKey: rsaKeys.exportKey('pkcs8-public-pem').replace(/\n/g, ''),
    };

    jest.spyOn(rsaKeys, 'encrypt').mockImplementationOnce(() => {
      return 'dupa';
    });

    expect(encryptService.encryptPdf(keys.pubKey, encryptService.getPdf())).toBe('dupa');
  });
});
