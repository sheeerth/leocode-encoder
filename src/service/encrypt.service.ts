import { Injectable } from '@nestjs/common';
import * as pdfToBase64 from 'pdf-to-base64';
import { Keys } from '../interface/keys';
import NodeRSA = require('node-rsa');

@Injectable()
export class EncryptService {
  generateKeys(): Keys {
    const rsaKeys = new NodeRSA();
    rsaKeys.generateKeyPair();

    const keys: Keys = {
      privKey: rsaKeys.exportKey('pkcs8-private-pem').replace(/\n/g, ''),
      pubKey: rsaKeys.exportKey('pkcs8-public-pem').replace(/\n/g, ''),
    };

    return keys;
  }

  async getPdf(): Promise<string> {
    return await pdfToBase64('http://www.africau.edu/images/default/sample.pdf');
  }

  encryptPdf(publicKey: string, pdfBase64): string {
    const tempKey = new NodeRSA();

    tempKey.importKey(publicKey, 'public');
    return tempKey.encrypt(pdfBase64, 'base64');
  }
}
