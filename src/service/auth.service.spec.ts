import { AuthService } from './auth.service';
import { UserMemoryRepository } from './user-memory-repository.service';
import * as jwtNode from 'jsonwebtoken';
import { env } from '../env';

describe('Auth service test', () => {
  let userService: UserMemoryRepository;
  let authService: AuthService;

  beforeEach(() => {
    userService = new UserMemoryRepository();
    authService = new AuthService(userService);
  });

  it('SERVICE INIT', () => {
    expect(authService).toBeInstanceOf(AuthService);
  });

  describe('[AUTH SERVICE] sign in', () => {
    test('[AUTH SERVICE] sign in - correct credentials', async () => {
      const email = 'test@test.pl';
      const password = 'Test12345';

      jest.spyOn(jwtNode, 'sign').mockImplementationOnce(() => {
        return 'token';
      });

      expect(await authService.signIn(email, password)).toStrictEqual({authToken: 'token'});
    });

    test('[AUTH SERVICE] sign in - incorrect credentials', async () => {
      const email = 'test@test.com';
      const password = 'Test123';

      expect(await authService.signIn(email, password)).toBe(null);
    });
  });

  describe('[AUTH SERVICE] verification Auth', () => {
    test('[AUTH SERVICE] verification Auth - correct verification', async () => {
      const jwtTokenMockUp = jwtNode.sign({email: 'test@test.pl'}, env.SECRET_KEY, {expiresIn: 300});

      jest.spyOn(jwtNode, 'verify').mockImplementationOnce(() => {
        return true;
      });

      expect(await authService.verificationAuth({headers: {authorization: jwtTokenMockUp}})).toBe(true);
    });
  });
});
