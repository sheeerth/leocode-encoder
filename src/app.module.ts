import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AuthService } from './service/auth.service';
import { AuthGuard } from './guard/auth.guard';
import { EncryptService } from './service/encrypt.service';
import { UserMemoryRepository } from './service/user-memory-repository.service';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [AuthService, AuthGuard, EncryptService, UserMemoryRepository],
})
export class AppModule {}
