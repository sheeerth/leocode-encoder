export interface User {
  email: string;
  password: string;
  publicKey?: string;
}
