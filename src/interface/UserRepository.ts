import { User } from './user';

export interface UserRepository {
  findUser(email: string): Promise<User>;

  findUserByJwtToken(jwtToken: string): Promise<User>;
}
