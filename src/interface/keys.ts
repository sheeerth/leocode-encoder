export interface Keys {
  pubKey: string;
  privKey: string;
}
