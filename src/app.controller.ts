import { BadRequestException, Body, Controller, Post, Request, UseGuards } from '@nestjs/common';
import { AuthService} from './service/auth.service';
import { AuthGuard } from './guard/auth.guard';
import { EncryptService } from './service/encrypt.service';
import { Request as Req } from 'express';
import { UserMemoryRepository } from './service/user-memory-repository.service';
import { Keys } from './interface/keys';
import { UserRepository } from './interface/UserRepository';

@Controller('api')
export class AppController {
  constructor(
    private readonly authService: AuthService,
    private readonly encryptService: EncryptService,
    private readonly userService: UserMemoryRepository) {}

  @Post('sign-in')
  async signIn(@Body('email') email: string, @Body('password') password: string): Promise<{ authToken: string }> {
    const response = await this.authService.signIn(email, password);

    if (response) {
      return response;
    }

    throw new BadRequestException();
  }

  @UseGuards(AuthGuard)
  @Post('generate-key-pair')
  async generateKeys(@Request() req: Req): Promise<Keys> {
    const user = await this.userService.findUserByJwtToken(req.headers.authorization);
    const keys = this.encryptService.generateKeys();

    user.publicKey = keys.pubKey;

    return keys;
  }

  @UseGuards(AuthGuard)
  @Post('encrypt')
  async getEncryptPdf(@Request() req: Req): Promise<string> {
    const user = await this.userService.findUserByJwtToken(req.headers.authorization);

    if (!user.publicKey) {
      throw new BadRequestException('Public key for this user is missing');
    }

    const base64Pdf =  await this.encryptService.getPdf();

    return this.encryptService.encryptPdf(user.publicKey, base64Pdf);
  }
}
