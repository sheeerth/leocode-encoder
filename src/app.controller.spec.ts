import { AppController } from './app.controller';
import { AuthService } from './service/auth.service';
import { EncryptService } from './service/encrypt.service';
import { UserMemoryRepository } from './service/user-memory-repository.service';
import * as jwtNode from 'jsonwebtoken';
import { env } from './env';
import { BadRequestException } from '@nestjs/common';

describe('App Controller Tests', () => {
  let appController: AppController;
  let authService: AuthService;
  let encryptService: EncryptService;
  let userService: UserMemoryRepository;

  beforeEach(() => {
    userService = new UserMemoryRepository();
    encryptService = new EncryptService();
    authService = new AuthService(userService);
    appController = new AppController(authService, encryptService, userService);
  });

  describe('testing sign-in endpoint', () => {
    const jwtMockUp = jwtNode.sign({email: 'test@test.pl'}, env.SECRET_KEY, {expiresIn: 300});

    test('Correct credentials', async () => {
      jest.spyOn(authService, 'signIn').mockImplementation((email: string, password: string) => new Promise((resolve, reject) => {
        resolve({authToken: jwtMockUp});
      }));

      expect(await appController.signIn('test@test.pl', 'Test12345')).toStrictEqual({authToken: jwtMockUp});
    });

    test('Incorrect email', async () => {
      jest.spyOn(authService, 'signIn').mockImplementation((email: string, password: string) => new Promise((resolve, reject) => {
        resolve(null);
      }));

      await expect(appController.signIn('test@test.com', 'Test12345')).rejects.toThrowError(BadRequestException);
    });

    test('Incorrect password', async () => {
      jest.spyOn(authService, 'signIn').mockImplementation((email: string, password: string) => new Promise((resolve, reject) => {
        resolve(null);
      }));

      await expect(appController.signIn('test@test.pl', 'Test1234')).rejects.toThrowError(BadRequestException);
    });
  });

  describe('testing generateKeys endpoint', () => {
    const jwtMockUp = jwtNode.sign({email: 'test@test.pl'}, env.SECRET_KEY, {expiresIn: 300});

    test('generate keys', async () => {
      const keys =  encryptService.generateKeys();
      jest.spyOn(encryptService, 'generateKeys').mockImplementation(() => {
        return keys;
      });

      expect(await appController.generateKeys({headers: {authorization: jwtMockUp}})).toStrictEqual(keys);
    });

    test('public keys for user', async () => {
      const keys =  encryptService.generateKeys();
      jest.spyOn(encryptService, 'generateKeys').mockImplementation(() => {
        return keys;
      });

      await appController.generateKeys({headers: {authorization: jwtMockUp}});

      const userMockUp = {
        email: 'test@test.pl',
        password: '662af1cd1976f09a9f8cecc868ccc0a2',
        publicKey: keys.pubKey,
      };

      expect(await userService.findUser('test@test.pl')).toStrictEqual(userMockUp);
    });
  });

  describe('testing encrypt endpoint', () => {
    const jwtMockUp = jwtNode.sign({email: 'test@test.pl'}, env.SECRET_KEY, {expiresIn: 300});

    test('return encrypt PDF file', async () => {
      const keys = encryptService.generateKeys();

      jest.spyOn(userService, 'findUserByJwtToken').mockImplementation((jwt: string) => {
        return new Promise((resolve, reject) => {
          resolve({
            email: 'test@test.pl',
            password: '662af1cd1976f09a9f8cecc868ccc0a2',
            publicKey: keys.pubKey,
          });
        });
      });

      jest.spyOn(encryptService, 'encryptPdf').mockImplementation(() => {
        return 'testowy';
      });

      expect(await appController.getEncryptPdf({headers: {authorization: jwtMockUp}})).toBe('testowy');
    });

    test('public keys not defined', async () => {
      await expect(appController.getEncryptPdf({headers: {authorization: jwtMockUp}})).rejects.toThrowError(BadRequestException);
    });
  });
});
